<!DOCTYPE html>
<html>
<head>
  <title>Create</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  
</head>
<body>	
	<div class="container">
		<form action="./f3il/create.php" method="post">
            
      <h1 class="mb-4 mt-5 text-center">Créer une tâche</h1>
      <?php if (isset($_GET['error'])) { ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $_GET['error']; ?>
        </div>
      <?php } ?>
      
      <div class="row">
        <div class="col-md-8" style="float: none;margin: 0 auto;">

          <div class="form-group">
            <label for="title">Titre *</label>
            <input type="title" 
            class="form-control" 
            id="title" 
            name="title"  
            placeholder="Enter le titre">
          </div>

          <div class="form-group">
            <label for="state">state *</label>
            <input type="state" 
              class="form-control"
              disabled
              id="state" 
              name="state" 
              placeholder="A faire">
          </div>

          <div class="form-group">
            <label for="datetime">Date et heure *</label>
            <input disabled name="datetime" id="datetime" type='text' class="form-control" />
          </div>

          <button type="submit" class="btn btn-primary" name="create">Créer</button>
          <a href="./index.php" class="link-primary">Voir la liste</a>
        </div>
      </div>
	  </form>
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
      document.getElementById('datetime').value=new Date().toUTCString();
    });
  </script>
</body>
</html>