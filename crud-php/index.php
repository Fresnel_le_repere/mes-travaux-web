<?php include "./f3il/read.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Create</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>	
	<div class="container">

    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Tâches à faire</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Tâches faites</a>
      </li>
    </ul><!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="tabs-1" role="tabpanel">
        <?php if (isset($_GET['success'])) { ?>
          <div class="col-4 toast bg-info" 
            style="color: white; background-color: ;max-width: 19%; padding: 1%; padding-left: 1%;"
            role="alert" 
            aria-live="assertive" 
            aria-atomic="true"
          >
            <div class="" style="padding-left: 3%;margin-bottom: 2%;">
              <i class="fa fa-info" style="padding: 1px;" ></i>
              <strong class="mr-auto text-white" style="padding: 1px;">Information</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div style="padding-left: 3%;" class="toast-body text-white">
              <?php if ($_GET['success'] === 'successfully created') {
                echo 'Tâche crée avec success';
              }else {
                echo 'Etat modifié avec success';
              }              
              ?>                
            </div>
          </div>
        <?php } ?>        
        <?php include './tasks-to-do.php'; ?>
      </div>
      <div class="tab-pane" id="tabs-2" role="tabpanel">
        <?php include './tasks-done.php'; ?>
      </div>
    </div>    
  </div>
<script>
$(document).ready(function() {
  $('.toast').toast({
    'autohide': false
  });

  $('.toast').toast('show');
});
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
</body>
</html>