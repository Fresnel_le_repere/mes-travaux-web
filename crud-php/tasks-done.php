<?php include "./f3il/read.php"; ?>
<div class="container">
  <!-- Bootstrap responsive table -->  
  <h1 class="mb-4 mt-5 text-center">Liste des tâches faites</h1>
  <!-- <button class="btn mb-2"><i class="fa fa-plus"></i></button> -->
  <?php if (mysqli_num_rows($result)) { ?>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Titre</th>
      <th scope="col">Etat</th>
      <th scope="col">Modifié le</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php 
    $i = 0;
    while($rows = mysqli_fetch_assoc($result)){
    $i++;
    if ($rows['etat'] == 'faites') {     
  ?>
    <tr>
      <th scope="row"><?=$rows['titre']; ?></th>
      <td><?=$rows['etat']; ?></td>
      <td><?=$rows['date_heure']; ?></td>
      <td>
        <a class="btn" href="./f3il/edit_state.php?id=<?=$rows['id']?>&state=<?=$rows['etat']?>" >
          <i class="fa fa-edit"></i>
        </a>
      </td>
    </tr>
  <?php }}?>    
  </tbody>
</table>
<?php } ?>